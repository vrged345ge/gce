﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using GCE.Repository;
using GCE.Service;
using GCE.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using GCE.Interfaces;
using Newtonsoft.Json;

namespace GCE.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var defaultConnection = Environment.GetEnvironmentVariable("DefaultConnection");
            
            defaultConnection = String.IsNullOrEmpty(defaultConnection) ? Configuration.GetConnectionString("DefaultConnection") : defaultConnection;

            services.AddDbContextPool<MainContext>(options => {
                options.UseMySql(defaultConnection);
            });

            services.AddCors(config => {
                config.AddPolicy("MyDevelopmentPolicy", builder =>
                {
                    builder
                    .AllowAnyOrigin() 
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
                });
            });

            services.AddTransient<IClienteService, ClienteService>();
            services.AddTransient<IContratoService, ContratoService>();
            services.AddTransient<IArquivoContratoService, ArquivoContratoService>();
            
            services.AddTransient<IContratoRepository, ContratoRepository>();
            services.AddTransient<IArquivoContratoRepository, ArquivoContratoRepository>();
            services.AddTransient<IClienteRepository, ClienteRepository>();

            services.AddOptions();
            services.AddMvcCore()
                    .AddJsonFormatters()
                    .AddJsonOptions(o => {
#if DEBUG
                        o.SerializerSettings.Formatting = Formatting.Indented;
#else
                        o.SerializerSettings.Formatting = Formatting.None;
#endif
                        o.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, MainContext mainContext)
        {
            mainContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }
            
            app.UseCors("MyDevelopmentPolicy");

            app.UseMvc();
        }
    }
}
