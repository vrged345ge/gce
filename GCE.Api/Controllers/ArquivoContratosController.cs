﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GCE.Model;
using GCE.Model.Dto.Api.Request;
using GCE.Service.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GCE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArquivoContratosController : ControllerBase
    {
        public readonly IArquivoContratoService _arquivoContratoService;
        private readonly IHostingEnvironment _hostingEnvironment;
        public ArquivoContratosController(IArquivoContratoService arquivoContratoService, IHostingEnvironment environment)
        {
            _arquivoContratoService = arquivoContratoService;
            _hostingEnvironment = environment;
        }

        [HttpGet("BuscarPorArquivo/{id}")]
        public ArquivoContrato Get(Guid id)
        {
            return _arquivoContratoService.Buscar(id);
        }

        [HttpGet("BuscarPorContrato/{id}")]
        public IList<ArquivoContrato> ListarPorContrato(Guid id)
        {
            return _arquivoContratoService.ListarPorContrato(id);
        }

        [HttpPost]
        public void Post([FromBody] List<ArquivoContratoRequest> arquivosContratos)
        {
            _arquivoContratoService.Criar(arquivosContratos);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] ArquivoContrato arquivoContrato)
        {
            _arquivoContratoService.Editar(arquivoContrato);
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _arquivoContratoService.Excluir(id);
        }

        [HttpPost("Upload")]
        public async Task<List<KeyValuePair<string, string>>> Upload(IList<IFormFile> files)
        {
            var uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "uploads");

            var list = new List<KeyValuePair<string, string>>();

            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var newFilename = Guid.NewGuid().ToString();
                    list.Add(new KeyValuePair<string, string>(file.FileName, newFilename));

                    var filePath = Path.Combine(uploads, newFilename);
                    //TODO: allow pdf extension only
                    //Path.GetExtension(filePath).ToLowerInvariant()

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
            }
            return list;
        }

        [HttpGet("Download/{id}")]
        public IActionResult Download(Guid id)
        {
            var result = _arquivoContratoService.Buscar(id);

            return File(result.Conteudo, "application/pdf", result.Nome);
        }
    }
}
