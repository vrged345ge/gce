﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GCE.Service.Interfaces;
using GCE.Model;
using Microsoft.AspNetCore.Mvc;
using GCE.Model.Dto.Api.Response;

namespace GCE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IClienteService _clienteService;
        public ClientesController(IClienteService clienteService) => _clienteService = clienteService;

        [HttpGet("{id}")]
        public BaseResponse<Cliente> Get(Guid id)
        {
            return _clienteService.get(id);
        }

        [HttpGet("buscarPorNome/{nome}")]
        public BaseResponse<IList<Cliente>> List(string nome)
        {
            return _clienteService.getByName(nome);
        }

        [HttpGet]
        public BaseResponse<IList<Cliente>> List()
        {
            return _clienteService.getAll();
        }

        [HttpPost]
        public BaseResponse<Cliente> Post([FromBody] Cliente cliente)
        {
            return _clienteService.create(cliente);
        }

        [HttpPut]
        public void Put([FromBody] Cliente cliente)
        {
            _clienteService.update(cliente);
        }

        [HttpDelete("{id}")]
        public BaseResponse<Cliente> Delete(Guid id)
        {
            return _clienteService.delete(new Cliente { Id = id });
        }
    }
}
