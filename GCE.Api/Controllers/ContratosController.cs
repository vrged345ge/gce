﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GCE.Model;
using GCE.Model.Dto.Api.Response;
using GCE.Service;
using GCE.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GCE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContratosController : ControllerBase
    {
        public readonly IContratoService _contratoService;
        public ContratosController(IContratoService contratoService) => _contratoService = contratoService;

        [HttpGet]
        public IList<Contrato> Listar()
        {
            return _contratoService.Listar();
        }

        [HttpGet("{id}")]
        public Contrato Get(Guid id)
        {
            return _contratoService.Buscar(id);
        }

        [HttpPost]
        public BaseResponse<Contrato> Post([FromBody] Contrato contrato)
        {
            return _contratoService.Criar(contrato);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] Contrato contrato)
        {
            _contratoService.Editar(contrato);
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _contratoService.Excluir(id);
        }
    }
}
