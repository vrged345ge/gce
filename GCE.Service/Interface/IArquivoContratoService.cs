using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;
using GCE.Model.Dto.Api.Request;
using GCE.Model.Dto.Api.Response;

namespace GCE.Service.Interfaces
{
    public interface IArquivoContratoService
    {
        List<ArquivoContrato> Criar(List<ArquivoContratoRequest> arquivoContratoRequest);
        ArquivoContrato Editar(ArquivoContrato arquivoContrato);
        void Excluir(Guid id);
        ArquivoContrato Buscar(Guid id);
        IList<ArquivoContrato> ListarPorContrato(Guid id);
    }
}