using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;
using GCE.Model.Dto.Api.Response;

namespace GCE.Service.Interfaces
{
    public interface IContratoService
    {
        BaseResponse<Contrato> Criar(Contrato contrato);
        BaseResponse<Contrato> Editar(Contrato contrato);
        Contrato Buscar(Guid id);
        BaseResponse<Contrato> Excluir(Guid id);
        IList<Contrato> Listar();
    }
}