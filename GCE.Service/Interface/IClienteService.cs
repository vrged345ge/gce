using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;
using GCE.Model.Dto.Api.Response;

namespace GCE.Service.Interfaces
{
    public interface IClienteService
    {
        BaseResponse<Cliente> get(Guid id);
        BaseResponse<IList<Cliente>> getByName(string nome);
        BaseResponse<IList<Cliente>> getAll();
        BaseResponse<Cliente> delete(Cliente cliente);
        BaseResponse<Cliente> update(Cliente cliente);
        BaseResponse<Cliente> create(Cliente cliente);
    }
}