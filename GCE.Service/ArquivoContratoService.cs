﻿using System;
using System.Collections.Generic;
using System.IO;
using GCE.Interfaces;
using GCE.Model;
using GCE.Model.Dto.Api.Request;
using GCE.Model.Dto.Api.Response;
using GCE.Service.Interfaces;
using Microsoft.AspNetCore.Hosting;

namespace GCE.Service
{
    public class ArquivoContratoService : IArquivoContratoService
    {
        public readonly IArquivoContratoRepository _arquivoContratoRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        public ArquivoContratoService(IArquivoContratoRepository arquivoContratoRepository, IHostingEnvironment environment)
        {
            _arquivoContratoRepository = arquivoContratoRepository;
            _hostingEnvironment = environment;
        }

        public ArquivoContrato Buscar(Guid id)
        {
            return _arquivoContratoRepository.Buscar(id);
        }

        public IList<ArquivoContrato> ListarPorContrato(Guid id)
        {
            return _arquivoContratoRepository.ListarPorContrato(id);
        }

        public List<ArquivoContrato> Criar(List<ArquivoContratoRequest> arquivosContratosRequests)
        {
            var arquivos = new List<ArquivoContrato>();
            foreach(var arquivoContrato in arquivosContratosRequests)
            {
                var uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "uploads");

                var filePath = Path.Combine(uploads, arquivoContrato.ArquivoId.ToString());

                var novoArquivoContrato = new ArquivoContrato {
                    ContratoId = arquivoContrato.ContratoId,
                    Nome = arquivoContrato.Nome,
                    Conteudo = File.ReadAllBytes(filePath)};
                
                arquivos.Add(_arquivoContratoRepository.Criar(novoArquivoContrato));

            }
            return arquivos;
        }

        public ArquivoContrato Editar(ArquivoContrato arquivoContrato)
        {
            return _arquivoContratoRepository.Editar(arquivoContrato);
        }

        public void Excluir(Guid id)
        {
            var arquivoEncontrado = _arquivoContratoRepository.Buscar(id);
            _arquivoContratoRepository.Excluir(arquivoEncontrado);
        }
    }
}