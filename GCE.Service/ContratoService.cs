﻿using System;
using System.Collections.Generic;
using GCE.Interfaces;
using GCE.Model;
using GCE.Model.Dto.Api.Response;
using GCE.Service.Interfaces;

namespace GCE.Service
{
    public class ContratoService : IContratoService
    {
        public readonly IContratoRepository _contratoRepository;
        public ContratoService(IContratoRepository contratoRepository) => _contratoRepository = contratoRepository;
    
        public Contrato Buscar(Guid id)
        {
            return _contratoRepository.Buscar(id);
        }
        public BaseResponse<Contrato> Criar(Contrato contrato)
        {
            var response = new BaseResponse<Contrato>();

            response.Data = _contratoRepository.Criar(contrato);

            return response;
        }

        public BaseResponse<Contrato> Editar(Contrato contrato)
        {
            var response = new BaseResponse<Contrato>();
            
            response.Data = _contratoRepository.Editar(contrato);

            return response;
        }

        public BaseResponse<Contrato> Excluir(Guid id)
        {
            var response = new BaseResponse<Contrato>();

            var contratoEncontrado = _contratoRepository.Buscar(id);
            _contratoRepository.Excluir(contratoEncontrado);

            return response;
        }

        public IList<Contrato> Listar()
        {
            return _contratoRepository.Listar();
        }
    }
}