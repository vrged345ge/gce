﻿using System;
using System.Collections.Generic;
using GCE.Interfaces;
using GCE.Model;
using GCE.Model.Dto.Api.Response;
using GCE.Service.Interfaces;

namespace GCE.Service
{
    public class ClienteService : IClienteService
    {
        public readonly IClienteRepository _clienteRepository;
        public ClienteService(IClienteRepository clienteRepository) => _clienteRepository = clienteRepository;

        public BaseResponse<Cliente> create(Cliente cliente)
        {
            var response = new BaseResponse<Cliente>();

            response.Data = _clienteRepository.Criar(cliente);

            return response;
        }

        private BaseResponse<Cliente> search(Cliente cliente)
        {
            var response = new BaseResponse<Cliente>();

            var clienteEncontrado = _clienteRepository.Buscar(cliente.Id);

            if(clienteEncontrado == null)
                response.Errors.Add(new ApiError(1, "Cliente não encontrado"));

            response.Data = clienteEncontrado;

            return response;
        }

        public BaseResponse<Cliente> delete(Cliente cliente)
        {
            var response = search(cliente);

            if(response.Errors.Count == 0)
                _clienteRepository.Excluir(response.Data);

            return response;
        }

        public BaseResponse<Cliente> get(Guid id)
        {
            return search(new Cliente { Id = id});
        }

        public BaseResponse<IList<Cliente>> getByName(string nome)
        {
            var response = new BaseResponse<IList<Cliente>>();

            response.Data = _clienteRepository.BuscarPorNome(nome);

            return response;
        }

        public BaseResponse<IList<Cliente>> getAll()
        {
            var response = new BaseResponse<IList<Cliente>>();

            response.Data = _clienteRepository.Listar();

            return response;
        }

        public BaseResponse<Cliente> update(Cliente cliente)
        {
            var response = search(cliente);

            if(response.Errors.Count == 0)
                _clienteRepository.Editar(response.Data);
            
            return response;
        }
    }
}