import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesComponent } from './clientes/clientes.component';
import { MaterialModule } from '../share/material.module';
import { ClienteModalComponent } from './cliente-modal/cliente-modal.component';
import { AutenticacaoComponent } from './autenticacao/autenticacao.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { ContratoComponent } from './contrato/contrato.component';
import { ContratoModalComponent } from './contrato-modal/contrato-modal.component';

@NgModule({
  declarations: [
    ClientesComponent,
    ClienteModalComponent,
    AutenticacaoComponent,
    ContratoComponent,
    ContratoModalComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule,
    MatDialogModule,
  ],
  exports: [
    ClientesComponent,
    ClienteModalComponent,
    AutenticacaoComponent,
  ],
  entryComponents: [ ClienteModalComponent, ContratoModalComponent ]
})
export class PagesModule { }
