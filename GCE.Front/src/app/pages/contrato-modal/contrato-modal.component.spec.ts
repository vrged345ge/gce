import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratoModalComponent } from './contrato-modal.component';

describe('ContratoModalComponent', () => {
  let component: ContratoModalComponent;
  let fixture: ComponentFixture<ContratoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
