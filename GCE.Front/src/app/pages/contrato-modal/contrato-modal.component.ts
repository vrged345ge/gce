import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ContratoService } from 'src/app/services/contrato.service';
import { Contrato } from 'src/app/model/contrato';
import { TipoContrato } from 'src/app/model/tipo-contrato';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/services/cliente.service';


@Component({
  selector: 'app-contrato-modal',
  templateUrl: './contrato-modal.component.html',
  styleUrls: ['./contrato-modal.component.sass']
})
export class ContratoModalComponent implements OnInit {

  myControl = new FormControl();
  options: Cliente[] = [];
  filteredOptions: Observable<Cliente[]>;
  selected = '1';

  public nome: string;

  constructor(
    public dialogRef: MatDialogRef<ContratoModalComponent>,
    public contratoService: ContratoService,
    public clienteService: ClienteService) { }

  ngOnInit() {
    this.clienteService.listar().subscribe(response => {
      this.options = response;
    });
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith<string | Cliente>(''),
      map(value => typeof value === 'string' ? value : value.nome),
      map(name => name ? this._filter(name) : this.options.slice())
    );
  }

  displayFn(cliente?: Cliente): string | undefined {
    return cliente ? cliente.nome : undefined;
  }

  private _filter(name: string): Cliente[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.nome.toLowerCase().indexOf(filterValue) === 0);
  }

  Salvar() {

    const contrato: Contrato = {
      clienteId: '',
      tipo: TipoContrato.Compra,
      quantidade: 10,
      valorNegociado: 10,
      inicio: new Date(),
      duracaoEmMeses: 10
    };

    this.contratoService.criar(contrato).subscribe(response => {

      this.dialogRef.close('salvo');
    });
  }

  Cancelar() {
    this.dialogRef.close();
  }
}
