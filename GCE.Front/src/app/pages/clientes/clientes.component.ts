import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { ClienteModalComponent } from '../cliente-modal/cliente-modal.component';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.sass']
})
export class ClientesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nome', 'actions'];
  dataSource: MatTableDataSource<Cliente>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public clienteService: ClienteService, public snackBarService: SnackBarService, public dialog: MatDialog) { }

  ngOnInit() {
    this.recarregarDataSource();
  }

  private recarregarDataSource() {
    this.clienteService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource<Cliente>(data);
    });
  }

  public Excluir(cliente: Cliente) {
    this.clienteService.excluir(cliente).subscribe(e => {
      this.recarregarDataSource();
      this.snackBarService.showIgnorableMessage('Cliente excluido com sucesso');
    });
  }

  public Novo() {
    const dialogRef = this.dialog.open(ClienteModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'salvo') {
        this.recarregarDataSource();
      }
    });
  }

}
