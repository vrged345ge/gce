import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/app/model/cliente';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-cliente-modal',
  templateUrl: './cliente-modal.component.html',
  styleUrls: ['./cliente-modal.component.sass']
})
export class ClienteModalComponent implements OnInit {

  public nome: string;

  constructor(
    public dialogRef: MatDialogRef<ClienteModalComponent>,
    public snackBarService: SnackBarService,
    public clienteService: ClienteService) {}

  ngOnInit() {
  }

  Salvar() {

    const cliente: Cliente = { nome: this.nome };

    this.clienteService.criar(cliente).subscribe(response => {

      this.dialogRef.close('salvo');
      this.snackBarService.showIgnorableMessage('Cliente criado com sucesso');
    });
  }

  Cancelar() {
    this.dialogRef.close();
  }

}
