import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ContratoService } from 'src/app/services/contrato.service';
import { Contrato } from 'src/app/model/contrato';
import { ContratoModalComponent } from '../contrato-modal/contrato-modal.component';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.sass']
})
export class ContratoComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nome', 'tipo', 'quantidade', 'valorNegociado', 'inicio', 'duracaoEmMeses', 'actions'];
  dataSource: MatTableDataSource<Contrato> = new MatTableDataSource<Contrato>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public contratoService: ContratoService, public dialog: MatDialog) { }

  ngOnInit() {
    this.recarregarDataSource();
  }

  private recarregarDataSource() {
    this.contratoService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource<Contrato>(data);
    });
  }

  public Excluir(contrato: Contrato) {
    this.contratoService.excluir(contrato).subscribe(e => {
      this.recarregarDataSource();
    });
  }

  public Novo() {
    const dialogRef = this.dialog.open(ContratoModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'salvo') {
        this.recarregarDataSource();
      }
    });
  }
}
