import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { AutenticacaoComponent } from './pages/autenticacao/autenticacao.component';
import { AuthGuard } from './guards/auth-guard.service';
import { ContratoComponent } from './pages/contrato/contrato.component';

const routes: Routes = [
  { path: 'clientes', component: ClientesComponent, canActivate: []},
  { path: 'contratos', component: ContratoComponent, canActivate: [] },
  // when i finished autentication page, then use: canActivate: [AuthGuard]
  { path: 'autenticacao', component: AutenticacaoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
