import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Cliente } from '../model/cliente';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }

  public listar(): Observable<Cliente[]> {
    return this.http.get(
      `${environment.API_URL}${environment.API_VERSION}Clientes`
    ).pipe(
      map(response => response['data'])
    );
  }

  public excluir(cliente: Cliente) {
    return this.http.delete(
      `${environment.API_URL}${environment.API_VERSION}Clientes/${cliente.id}`
    );
  }

  public criar(cliente: Cliente) {
    return this.http.post(`${environment.API_URL}${environment.API_VERSION}Clientes`, cliente);
  }
}
