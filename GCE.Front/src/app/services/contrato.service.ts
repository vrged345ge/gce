import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contrato } from '../model/contrato';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  constructor(private http: HttpClient) { }

  public listar(): Observable<Contrato[]> {
    return this.http.get(
      `${environment.API_URL}${environment.API_VERSION}Contratos`
    ).pipe(
      map(response => response['data'])
    );
  }

  public excluir(contrato: Contrato) {
    return this.http.delete(
      `${environment.API_URL}${environment.API_VERSION}Contratos/${contrato.id}`
    );
  }

  public criar(contrato: Contrato) {
    return this.http.post(`${environment.API_URL}${environment.API_VERSION}Contratos`, contrato);
  }
}
