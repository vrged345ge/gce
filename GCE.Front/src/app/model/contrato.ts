import { ArquivoContrato } from './arquivo-contrato';
import { TipoContrato } from './tipo-contrato';

export interface Contrato {
  id?: string;
  clienteId: string;
  tipo: TipoContrato;
  quantidade: number;
  valorNegociado: number;
  inicio: Date;
  duracaoEmMeses: number;
  arquivosContrato?: ArquivoContrato[];
}
