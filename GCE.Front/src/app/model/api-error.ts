export interface ApiError {
  code: number;
  field: string;
  description: string;
}
