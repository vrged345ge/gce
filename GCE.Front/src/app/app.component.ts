import { Component, ApplicationRef } from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import {
  BreakpointObserver
} from '@angular/cdk/layout';
import { interval } from 'rxjs/internal/observable/interval';
import { concat } from 'rxjs/internal/observable/concat';
import { first } from 'rxjs/operators';
import 'hammerjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  public isOpened = true;
  title = 'Gestão de Contratos de Energia';
  public screenWidth: number;

  constructor(private http: HttpClient) {
    this.screenWidth = window.innerWidth;

    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }

  public isMobile(): boolean {
    return this.screenWidth > 840;
  }

  isMobileSidenavMode() {
    return this.isMobile() ? 'side' : 'over';
  }
}
