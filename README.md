# Para fazer a validação do que foi realizar, é necessário seguir os passos:

# executar no terminal:
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=password -p 3306:3306 -d mysql:8.0

# no projeto:
# GCE.Api
# executar:
dotnet run

# No projeto:
# GCE.Front
# executar:
ng serve

# Dependencias para realizar teste:

docker
dotnet core 2.2
@angular/cli@8.0.0-beta.15

# O que foi finalizado:

- CRUD Backend completo das entidades.
- CRD Frontend para entidade Cliente.
- Somente layout inicial da tela de login do usuário

# O que faltou:

- CRUD Frontend contrato
- Uma tela de login do usuário
- Um sistema de busca
- Uma validação do contrato
