﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GCE.Interfaces;
using GCE.Model;

namespace GCE.Repository
{
    public class ArquivoContratoRepository : IArquivoContratoRepository
    {
        private readonly MainContext _context;

        public ArquivoContratoRepository(MainContext context) => _context = context;
        
        public ArquivoContrato Buscar(Guid id)
        {
            return _context.ArquivoContratos.FirstOrDefault(e => e.Id == id);
        }

        public ArquivoContrato Criar(ArquivoContrato arquivoContrato)
        {
            _context.ArquivoContratos.Add(arquivoContrato);
            _context.SaveChanges();

            return arquivoContrato;
        }

        public ArquivoContrato Editar(ArquivoContrato contrato)
        {
            _context.ArquivoContratos.Update(contrato);
            _context.SaveChanges();

            return contrato;
        }

        public void Excluir(ArquivoContrato arquivoContrato)
        {
            _context.ArquivoContratos.Remove(arquivoContrato);
            _context.SaveChanges();
        }

        public IList<ArquivoContrato> Listar()
        {
            return _context.ArquivoContratos.ToList();
        }

        public IList<ArquivoContrato> ListarPorContrato(Guid idContrato)
        {
            return _context.ArquivoContratos.Where(e => e.ContratoId == idContrato).ToList();
        }
    }
}
