using System;
using GCE.Model;
using Microsoft.EntityFrameworkCore;

namespace GCE.Repository
{
    public class MainContext : DbContext
    {
        public MainContext(DbContextOptions<MainContext> options) : base(options) { }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<ArquivoContrato> ArquivoContratos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Cliente>().HasIndex(c => c.Nome).IsUnique();
        }
    }
}