using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;

namespace GCE.Interfaces
{
    public interface IClienteRepository
    {
        Cliente Criar(Cliente cliente);
        Cliente Editar(Cliente cliente);
        void Excluir(Cliente cliente);
        Cliente Buscar(Guid id);
        IList<Cliente> BuscarPorNome(string nome);
        IList<Cliente> Listar();
    }
}