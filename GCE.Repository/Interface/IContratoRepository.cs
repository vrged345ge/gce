using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;

namespace GCE.Interfaces
{
    public interface IContratoRepository
    {
        Contrato Criar(Contrato contrato);
        Contrato Editar(Contrato contrato);
        void Excluir(Contrato contrato);
        IList<Contrato> Listar();
        Contrato Buscar(Guid id);
    }
}