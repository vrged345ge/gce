using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GCE.Model;

namespace GCE.Interfaces
{
    public interface IArquivoContratoRepository
    {
        ArquivoContrato Criar(ArquivoContrato contrato);
        ArquivoContrato Editar(ArquivoContrato contrato);
        void Excluir(ArquivoContrato contrato);
        IList<ArquivoContrato> Listar();
        IList<ArquivoContrato> ListarPorContrato(Guid idContrato);
        ArquivoContrato Buscar(Guid id);
    }
}