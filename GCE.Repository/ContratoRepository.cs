﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GCE.Interfaces;
using GCE.Model;

namespace GCE.Repository
{
    public class ContratoRepository : IContratoRepository
    {
        private readonly MainContext _context;

        public ContratoRepository(MainContext context) => _context = context;

        public void Excluir(Contrato contrato)
        {
            _context.Contratos.Remove(contrato);
            _context.SaveChanges();
        }

        public Contrato Editar(Contrato contrato)
        {
            _context.Contratos.Update(contrato);
            _context.SaveChanges();

            return contrato;
        }

        public Contrato Buscar(Guid id)
        {
            return _context.Contratos.FirstOrDefault(e => e.Id == id);
        }

        public IList<Contrato> Listar()
        {
            return _context.Contratos.ToList();
        }

        public Contrato Criar(Contrato contrato)
        {
            _context.Contratos.Add(contrato);
            _context.SaveChanges();

            return contrato;
        }
    }
}
