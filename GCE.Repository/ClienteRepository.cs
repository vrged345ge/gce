﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GCE.Interfaces;
using GCE.Model;

namespace GCE.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly MainContext _context;

        public ClienteRepository(MainContext context) => _context = context;

        public void Excluir(Cliente cliente)
        {
            _context.Clientes.Remove(cliente);
            _context.SaveChanges();
        }

        public Cliente Editar(Cliente cliente)
        {
            _context.Clientes.Update(cliente);
            _context.SaveChanges();

            return cliente;
        }

        public Cliente Buscar(Guid id)
        {
            return _context.Clientes.FirstOrDefault(e => e.Id == id);
        }

        public IList<Cliente> BuscarPorNome(string nome)
        {
            return _context.Clientes.Where(e => e.Nome.StartsWith(nome)).ToList();
        }

        public IList<Cliente> Listar()
        {
            return _context.Clientes.ToList();
        }

        public Cliente Criar(Cliente cliente)
        {
            _context.Clientes.Add(cliente);
            _context.SaveChanges();

            return cliente;
        }
    }
}
