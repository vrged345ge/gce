﻿// <auto-generated />
using System;
using GCE.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GCE.Repository.Migrations
{
    [DbContext(typeof(MainContext))]
    partial class MainContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("GCE.Model.ArquivoContrato", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte[]>("Conteudo")
                        .HasMaxLength(83886080);

                    b.Property<Guid>("ContratoId");

                    b.Property<string>("Nome")
                        .HasMaxLength(260);

                    b.HasKey("Id");

                    b.HasIndex("ContratoId");

                    b.ToTable("ArquivoContratos");
                });

            modelBuilder.Entity("GCE.Model.Cliente", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome")
                        .HasMaxLength(2048);

                    b.HasKey("Id");

                    b.ToTable("Clientes");
                });

            modelBuilder.Entity("GCE.Model.Contrato", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ClienteId");

                    b.Property<int>("DuracaoEmMeses");

                    b.Property<DateTimeOffset>("Inicio");

                    b.Property<int>("Quantidade");

                    b.Property<int>("Tipo");

                    b.Property<decimal>("ValorNegociado");

                    b.HasKey("Id");

                    b.HasIndex("ClienteId");

                    b.ToTable("Contratos");
                });

            modelBuilder.Entity("GCE.Model.ArquivoContrato", b =>
                {
                    b.HasOne("GCE.Model.Contrato")
                        .WithMany("Arquivos")
                        .HasForeignKey("ContratoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GCE.Model.Contrato", b =>
                {
                    b.HasOne("GCE.Model.Cliente", "Cliente")
                        .WithMany("Contratos")
                        .HasForeignKey("ClienteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
