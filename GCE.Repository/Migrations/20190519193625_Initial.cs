﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GCE.Repository.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contratos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ClienteId = table.Column<Guid>(nullable: false),
                    Tipo = table.Column<int>(nullable: false),
                    Quantidade = table.Column<int>(nullable: false),
                    ValorNegociado = table.Column<decimal>(nullable: false),
                    Inicio = table.Column<DateTimeOffset>(nullable: false),
                    DuracaoEmMeses = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contratos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contratos_Clientes_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Clientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArquivoContratos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContratoId = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(maxLength: 260, nullable: true),
                    Conteudo = table.Column<byte[]>(maxLength: 83886080, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArquivoContratos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArquivoContratos_Contratos_ContratoId",
                        column: x => x.ContratoId,
                        principalTable: "Contratos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArquivoContratos_ContratoId",
                table: "ArquivoContratos",
                column: "ContratoId");

            migrationBuilder.CreateIndex(
                name: "IX_Contratos_ClienteId",
                table: "Contratos",
                column: "ClienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArquivoContratos");

            migrationBuilder.DropTable(
                name: "Contratos");

            migrationBuilder.DropTable(
                name: "Clientes");
        }
    }
}
