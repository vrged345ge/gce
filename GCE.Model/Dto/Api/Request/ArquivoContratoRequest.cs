using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GCE.Model.Dto.Api.Request
{
    public class ArquivoContratoRequest
    {
        public Guid ContratoId { get; set; }
        [MaxLength(260)]
        public string Nome { get; set; }
        public Guid ArquivoId { get; set; }
    }
}

