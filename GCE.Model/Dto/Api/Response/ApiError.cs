using System;

namespace GCE.Model.Dto.Api.Response
{
    public class ApiError
    {
        public ApiError(long code, string description){
            this.code = code;
            this.description = description;
        }
        public ApiError(long code, string field, string description): this(code, description){
            
            this.field = field;
        }
        
        public long code;
        public string field;
        public string description;
    }
}
