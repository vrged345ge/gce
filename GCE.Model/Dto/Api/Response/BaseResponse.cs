using System;
using System.Collections.Generic;

namespace GCE.Model.Dto.Api.Response
{
    public class BaseResponse<T>
    {
        public T Data;
        public ICollection<ApiError> Errors = new List<ApiError>();
    }
}

