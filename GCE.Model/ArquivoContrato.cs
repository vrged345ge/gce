﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace GCE.Model
{
    public class ArquivoContrato
    {
        public Guid Id { get; set; }

        public Guid ContratoId { get; set; }
        
        [MaxLength(260)]
        public string Nome { get; set; }
        // 80Mb
        [MaxLength(83886080)]
        [JsonIgnore]
        public byte[] Conteudo { get; set; }
    }
}
