﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GCE.Model
{
    public class Cliente
    {
        public Guid Id { get; set; }
        [MaxLength(2048)]
        public string Nome { get; set; }
        public ICollection<Contrato> Contratos { get; set; }
    }
}
