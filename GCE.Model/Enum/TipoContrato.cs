using System;

namespace GCE.Model.Enum
{
    public enum TipoContrato
    {
        Compra,
        Venda
    }
}
