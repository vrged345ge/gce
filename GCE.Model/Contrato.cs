﻿using System;
using System.Collections.Generic;
using GCE.Model.Enum;

namespace GCE.Model
{
    public class Contrato
    {
        public Guid Id { get; set; }
        public Guid ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
        public TipoContrato Tipo { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorNegociado { get; set; }
        public DateTimeOffset Inicio { get; set; }
        public int DuracaoEmMeses { get; set; }
        public ICollection<ArquivoContrato> Arquivos { get; set; }
    }
}
